#!/bin/bash

if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
    echo "script ${BASH_SOURCE[0]} is being sourced ..."
else
    echo "load as source"
    echo ". $BASH_SOURCE"
fi

python3_virt_env () {

    # create and activate python virtual environment
    echo 'python3_virt_env'

    # is venv currently active? same location?
    # deactivate if different location than where this was called

    # VIRTUAL_ENV is created with venv/bin/activate
    if [[ -v VIRTUAL_ENV ]]; then
        # does the virtual directory exist
        if [ -d "$VIRTUAL_ENV" ]; then
            local _source=$(realpath $VIRTUAL_ENV/..)
            # compare current path with virtual_env path
            if [[ $PWD = $_source* ]]; then
                # the current pwd is inside virtual_env
                echo 'is activated'
                return 0
            else
                # the current pwd is outside virtual_env
                echo 'venv changed paths; deactivate'
                deactivate
            fi
        else
            echo 'venv was activated and is missing; deactivate'
            deactivate
        fi
    fi

    if ! [ -d "venv" ]; then
        echo "creating new venv"
        python3 -m venv venv
    fi

    echo 'activated'
    . "$PWD"/venv/bin/activate
}

python3_virt_env
